package ci.kossovo.hotel.email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserJms {
	
	private String name;
	private String usename;
	private String mail;
	private String password;
	
	@Override
	public String toString() {
		return "UserJms [name=" + name + ", usename=" + usename + ", mail=" + mail + ", password=" + password + "]";
	}
	
	

}
