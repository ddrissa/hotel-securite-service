package ci.kossovo.hotel.entities.num;

public enum NomRole {
	ROLE_USER,
    ROLE_ADMIN,
    ROLE_EMPLOYE,
    ROLE_CLIENT,
    ROLE_GERANT,
    ROLE_DBA,
    ROLE_STAGIAIRE
}
