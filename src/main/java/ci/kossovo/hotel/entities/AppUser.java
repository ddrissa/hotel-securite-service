package ci.kossovo.hotel.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import ci.kossovo.hotel.entities.audit.DateAudit;

@Entity
@Table(name = "USERS", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
		@UniqueConstraint(columnNames = { "email" }) })
public class AppUser extends DateAudit {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "user_sequence")
	@SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", initialValue = 100)
	private Long id;

	@NotBlank
	@Size(max = 40)
	private String titulaire;

	@NotBlank
	@Size(max = 40)
	private String name;

	@NotBlank
	@Size(max = 15)
	private String username;

	@NotBlank
	@Email
	@Size(max = 40)
	private String email;
	@NotBlank
	@Size(min = 4, max=100)
	private String password;

	/*
	 * @ManyToMany(fetch=FetchType.LAZY)
	 * 
	 * @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
	 * inverseJoinColumns = @JoinColumn(name = "role_id")) private Set<AppRole>
	 * roles = new HashSet<>();
	 */

	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;
	private boolean provisoire = false;

	public AppUser() {
		super();
	}

	public AppUser(@NotBlank String titulaire, @NotBlank String name, @NotBlank String username,
			@NotBlank @Email String email, @NotBlank @Size(min = 4) String password) {
		this.titulaire = titulaire;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isProvisoire() {
		return provisoire;
	}

	public void setProvisoire(boolean provisoire) {
		this.provisoire = provisoire;
	}

}
