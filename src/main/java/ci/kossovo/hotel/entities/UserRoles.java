package ci.kossovo.hotel.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@NoArgsConstructor
@ToString(of = { "user", "role" })
@Table(name="USERS_ROLES", uniqueConstraints= {@UniqueConstraint(columnNames= {"USER_ID","ROLE_ID"})})
public class UserRoles implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private AppUser user;
	
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private AppRole role;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public AppRole getRole() {
		return role;
	}

	public void setRole(AppRole role) {
		this.role = role;
	}

	public UserRoles(AppUser user, AppRole role) {
		this.user = user;
		this.role = role;
	}

	

}
