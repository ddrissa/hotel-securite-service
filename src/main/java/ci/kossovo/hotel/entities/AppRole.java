package ci.kossovo.hotel.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NaturalId;

import ci.kossovo.hotel.entities.num.NomRole;

@Entity
@Table(name = "ROLES", uniqueConstraints = { @UniqueConstraint(columnNames = { "nom" }) })
public class AppRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Enumerated(EnumType.STRING)
	@NaturalId
	@Column(length = 60)
	private NomRole nom;

	private String description;

	public AppRole() {
		super();
	}

	public AppRole(NomRole nom, String desc) {
		this.nom = nom;
		this.description = desc;
	}

	public AppRole(NomRole nom) {
		this.nom = nom;
	}

	public NomRole getNom() {
		return nom;
	}

	public void setNom(NomRole nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
