package ci.kossovo.hotel.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ci.kossovo.hotel.email.UserJms;
import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.AppUser;
import ci.kossovo.hotel.entities.UserRoles;
import ci.kossovo.hotel.entities.num.NomRole;
import ci.kossovo.hotel.exception.AppException;
import ci.kossovo.hotel.payload.ApiReponse;
import ci.kossovo.hotel.payload.JwtAuthenticationResponse;
import ci.kossovo.hotel.payload.LoginRequest;
import ci.kossovo.hotel.payload.SignUpRequest;
import ci.kossovo.hotel.repository.RoleRepository;
import ci.kossovo.hotel.repository.UserRepository;
import ci.kossovo.hotel.repository.UserRoleRepository;
import ci.kossovo.hotel.security.JwtTokenProvider;

@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class AuthController {

	private AuthenticationManager authenticationManager;
	private JwtTokenProvider jwtTokenProvider;
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private UserRoleRepository userRoleRepository;
	private PasswordEncoder passwordEncoder;
	private JmsTemplate jmsTemplate;

	public AuthController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider,
			UserRepository userRepository, RoleRepository roleRepository, UserRoleRepository userRoleRepository,
			PasswordEncoder passwordEncoder, JmsTemplate jmsTemplate) {
		this.authenticationManager = authenticationManager;
		this.jwtTokenProvider = jwtTokenProvider;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.userRoleRepository = userRoleRepository;
		this.passwordEncoder = passwordEncoder;
		this.jmsTemplate = jmsTemplate;
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authentifierUser(@Valid @RequestBody LoginRequest loginRequest) {
		try {
			Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
			
			System.out.println("########################");
			System.out.println(authentication.toString());
			SecurityContextHolder.getContext().setAuthentication(authentication);

			
			String jwt = jwtTokenProvider.generateToken(authentication);

			return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
		} catch (AuthenticationException e) {
			System.out.println("########### errreur passs #############");
			return new ResponseEntity<>(new ApiReponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("/signup")
	public ResponseEntity<?> inscrireUser(@Valid @RequestBody SignUpRequest signUpRequest) {

		System.out.println("*************************");
		System.out.println(signUpRequest.toString());

		if (!signUpRequest.getConfirmPassword().equals(signUpRequest.getPassword())) {
			return new ResponseEntity<>(new ApiReponse(false, "Les mots de passe ne correspondent pas."),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiReponse(false, "L'username existe deja."), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ApiReponse(false, "L'email existe deja."), HttpStatus.BAD_REQUEST);
		}
		if (userRepository.existsByTitulaire(signUpRequest.getCni())) {
			return new ResponseEntity<>(new ApiReponse(false, "Ce titulaire a deja un compte."),
					HttpStatus.BAD_REQUEST);
		}

		AppUser appUser = new AppUser(signUpRequest.getCni(), signUpRequest.getName(), signUpRequest.getUsername(),
				signUpRequest.getEmail(), signUpRequest.getPassword());
		System.out.println("************BT*************");
		System.out.println(signUpRequest.getPassword());
		appUser.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
		AppRole r = roleRepository.findByNom(NomRole.ROLE_USER)
				.orElseThrow(() -> new AppException("Ce role " + NomRole.ROLE_USER.name() + " n'est pas defini!"));

		System.out.println("************1*************");
		AppUser resultat = userRepository.save(appUser);

		userRoleRepository.save(new UserRoles(resultat, r));

		UserJms userJms = new UserJms(resultat.getName(), resultat.getUsername(), resultat.getEmail(),
				signUpRequest.getPassword());

		jmsTemplate.convertAndSend("hotel", userJms);

		URI emplacement = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(resultat.getUsername()).toUri();

		return ResponseEntity.created(emplacement).body(new ApiReponse(true, "Utilisateur enregistré avec succès."));

	}
}
