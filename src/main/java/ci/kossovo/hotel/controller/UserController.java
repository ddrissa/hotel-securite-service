package ci.kossovo.hotel.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.AppUser;
import ci.kossovo.hotel.entities.UserRoles;
import ci.kossovo.hotel.entities.num.NomRole;
import ci.kossovo.hotel.exception.ResourceNotFoundException;
import ci.kossovo.hotel.payload.ApiReponse;
import ci.kossovo.hotel.payload.RoleRequest;
import ci.kossovo.hotel.payload.UserCurrent;
import ci.kossovo.hotel.payload.UserIdentityAvailability;
import ci.kossovo.hotel.payload.UserProfile;
import ci.kossovo.hotel.payload.UserRequest;
import ci.kossovo.hotel.payload.UserSummary;
import ci.kossovo.hotel.repository.RoleRepository;
import ci.kossovo.hotel.repository.UserRepository;
import ci.kossovo.hotel.repository.UserRoleRepository;
import ci.kossovo.hotel.security.CurrentUser;
import ci.kossovo.hotel.security.UserPrincipal;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class UserController {

	private UserRepository userRepository;
	private UserRoleRepository userRoleRepository;
	private RoleRepository roleRepository;
	private PasswordEncoder passwordEncoder;

	public UserController(UserRepository userRepository, UserRoleRepository userRoleRepository,
			RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.userRoleRepository = userRoleRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(),
				currentUser.getName());
		return userSummary;
	}

	@GetMapping("/user/checkUsenemeDisponible")
	public UserIdentityAvailability checkUsernameDisponible(@RequestParam(value = "username") String username) {
		Boolean dispo = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(dispo);
	}

	@GetMapping("/user/checkEmailDisponible")
	public UserIdentityAvailability checkEmailDisponible(@RequestParam(value = "email") String email) {
		Boolean dispo = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(dispo);
	}

	@GetMapping("/users/{username}")
	public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
		AppUser user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
		return new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getCreatedAt());
	}

	@PutMapping("/users/dba")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public ResponseEntity<?> modifier(@Valid @RequestBody UserRequest ur) {
		// Aller chercher user dans la base.
		AppUser user = userRepository.findById(ur.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", ur.getId()));

		user.setTitulaire(ur.getTitulaire());
		user.setAccountNonExpired(ur.isAccountNonExpired());
		user.setAccountNonLocked(ur.isAccountNonLocked());
		user.setEnabled(ur.isEnabled());

		// A completer

		try {
			user = userRepository.save(user);
		} catch (Exception e) {

			return new ResponseEntity<>(new ApiReponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.ok(new ApiReponse(true, "User modifié est  " + user.getUsername()));

	}

	@GetMapping("/user/current/{username}")
	@PreAuthorize("#current.username == auhentication.principal.username")
	public UserCurrent getUserCurrent(@PathVariable(value = "username") String username) {
		AppUser u = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		return new UserCurrent(u.getId(), u.getName(), u.getUsername(), u.getEmail(), u.getPassword());
	}

	// Modification par user
	@PutMapping("/user/modifie")
	@PreAuthorize("#current.username == auhentication.principal.username")
	public ResponseEntity<?> modifieUser(@RequestBody UserCurrent current) {
		AppUser user = userRepository.findById(current.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", current.getName()));

		user.setName(current.getName());
		user.setUsername(current.getUsername());
		user.setEmail(current.getEmail());
		user.setPassword(passwordEncoder.encode(current.getPassword()));
		AppUser us = null;

		try {
			us = userRepository.save(user);
		} catch (Exception e) {
			return new ResponseEntity<>(new ApiReponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.ok(new ApiReponse(true, "User modifié est  " + us.getUsername()));

	}

	@GetMapping("/users/dba")
	@PreAuthorize("hasRole('GERANT') OR hasRole('DBA')")
	public List<AppUser> getUsers() {

		return userRepository.findAll();

	}

	@GetMapping("/users/dba/{username}")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public UserRequest getUserDba(@PathVariable(value = "username") String username) {
		AppUser u = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		return new UserRequest(u.getId(), u.getTitulaire(), u.getName(), u.getUsername(), u.getEmail(),
				u.isAccountNonExpired(), u.isAccountNonLocked(), u.isCredentialsNonExpired(), u.isEnabled());
	}

	@PutMapping("/users/dba/addroles")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public ResponseEntity<?> addRoles(@RequestBody RoleRequest roleRequest) {
		
		System.out.println("###################");
		System.out.println(roleRequest.getRoles());
		AppUser user = userRepository.findById(roleRequest.getIdUser())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", roleRequest.getIdUser()));
		List<String>roleDef= new ArrayList<>();
		roleRequest.getRoles().forEach(rs -> roleDef.add("ROLE_"+rs));
		
		roleDef.stream().forEach(r -> {
			NomRole nr= NomRole.valueOf(r);
			AppRole role = roleRepository.findByNom(nr)
					.orElseThrow(() -> new ResourceNotFoundException("Role", "nom role", r));
			UserRoles ur = new UserRoles(user, role);
			if (!userRoleRepository.existsByUserAndRole(user, role)) {
				userRoleRepository.save(ur);
			}

		});

		return ResponseEntity.ok(new ApiReponse(true, "Les role ont été modifié pour user  " + user.getUsername()));
	}
	
	@DeleteMapping("/users/dba/supproles/{id}/{role}")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public ResponseEntity<?> suppRoles(@PathVariable Long id, @PathVariable String role) {
		AppUser user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", id));
		AppRole r= roleRepository.findByNom(role)
				.orElseThrow(()-> new ResourceNotFoundException("Role", "Le role", role));
		UserRoles userRole= new UserRoles(user, r);
		UserRoles us = userRoleRepository.findByUserAndRole(user, r)
				.orElseThrow(()-> new ResourceNotFoundException("UserRole", "Le userRolr", userRole));
		userRoleRepository.delete(us);
		
		return ResponseEntity.ok(new ApiReponse(true, "Les role de user a ete supprimer avec succes: " + us));
	}
}
