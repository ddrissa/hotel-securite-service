package ci.kossovo.hotel.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

@Component
public class JwtTokenProvider {
	private static Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

//Champs pour califier les roles
	private boolean isAdmin = false;
	private boolean isEmploye = false;
	private boolean isClient = false;
	private boolean isUser = false;
	private boolean isGerant = false;
	private boolean isDba = false;
	private boolean isStagiaire = false;

	public String generateToken(Authentication authentication) {
		
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		userPrincipal.getAuthorities().forEach(r -> {
			if (r.getAuthority() == "ROLE_ADMIN")
				isAdmin = true;
			if (r.getAuthority() == "ROLE_EMPLOYE")
				isEmploye = true;
			if (r.getAuthority() == "ROLE_CLIENT")
				isClient = true;
			if (r.getAuthority() == "ROLE_GERANT")
				isGerant = true;
			if (r.getAuthority() == "ROLE_USER")
				isUser = true;
			if (r.getAuthority() == "ROLE_DBA")
				isDba = true;
			if (r.getAuthority() == "ROLE_STAGIAIRE")
				isStagiaire = true;
		});

		Date now = new Date();
		Date dateExpiration = new Date(now.getTime() + jwtExpirationInMs);

		List<String> roles = new ArrayList<>();
		userPrincipal.getAuthorities().forEach(a -> roles.add(a.getAuthority()));

		return JWT.create().withSubject(Long.toString(userPrincipal.getId())).withIssuedAt(new Date())
				.withExpiresAt(dateExpiration).withArrayClaim("roles", roles.toArray(new String[roles.size()]))
				.withClaim("isAdmin", isAdmin).withClaim("isEmploye", isEmploye).withClaim("isClient", isClient)
				.withClaim("isGerant", isGerant).withClaim("isUser", isUser).withClaim("isDba", isDba)
				.withClaim("isStagiaire", isStagiaire).sign(Algorithm.HMAC512(jwtSecret));

	}

	public Long getUserIdFromJWT(String token) {

		Algorithm algorithm = Algorithm.HMAC512(jwtSecret);
		JWTVerifier verifier = JWT.require(algorithm).build();
		DecodedJWT jwt = verifier.verify(token);

		return Long.parseLong(jwt.getSubject());
	}

	public boolean validateToken(String authToken) {

		try {

			JWT.require(Algorithm.HMAC512(jwtSecret)).build().verify(authToken);
			return true;

		} catch (JWTVerificationException e) {
			logger.error(e.getMessage());
		}

		return false;

	}

}
