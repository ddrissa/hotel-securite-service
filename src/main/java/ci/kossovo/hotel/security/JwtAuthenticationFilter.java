package ci.kossovo.hotel.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import ci.kossovo.hotel.util.ConstantSecurite;

public class JwtAuthenticationFilter extends OncePerRequestFilter {
	private static final Logger logger= LoggerFactory.getLogger(JwtAuthenticationFilter.class);
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private PersoUserDetailsService persoUserDetailsService;
	

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		try {
			String jwt = getJwtFromRequest(request);
			
			if (StringUtils.hasText(jwt) && jwtTokenProvider.validateToken(jwt)) {
				Long userId= jwtTokenProvider.getUserIdFromJWT(jwt);
				
				/*
				 * Au lieu de construire userDetail a partir de jwt en extrayant unsername et
				 * roles du token on a preferer aller chercher dans la base de donnee.
				 */
				UserDetails userDetails = persoUserDetailsService.loadUserById(userId);
				UsernamePasswordAuthenticationToken authenticationToken= new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		} catch (Exception e) {
			logger.error("Impossible de définir l'authentification de l'utilisateur dans un contexte de sécurité", e);
		}
		
		filterChain.doFilter(request, response);

	}

	private String getJwtFromRequest(HttpServletRequest request) {
		String hotelToken = request.getHeader(ConstantSecurite.TOKEN_HEADER);
		if (StringUtils.hasText(hotelToken) && hotelToken.startsWith(ConstantSecurite.TOKEN_PREFIX)) {
			return hotelToken.substring(ConstantSecurite.TOKEN_PREFIX.length(), hotelToken.length());
		}

		return null;
	}

}
