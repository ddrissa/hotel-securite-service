package ci.kossovo.hotel.security;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.AppUser;
import ci.kossovo.hotel.repository.UserRepository;

@Service
public class PersoUserDetailsService implements UserDetailsService {
	
	private UserRepository userRepository;
	

	public PersoUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Transactional
	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		AppUser user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).orElseThrow(() ->
		new UsernameNotFoundException("Utilisateur non trouvé avec nom d'utilisateur ou email: "+usernameOrEmail));
		
		List<AppRole> roles = userRepository.getRoles(user.getId());
		
		return UserPrincipal.create(user, roles);
	}
	
	// Cette méthode est utilisée par JWTAuthenticationFilter
	public UserDetails loadUserById(Long id) throws UsernameNotFoundException {
		AppUser user = userRepository.findById(id).orElseThrow(() ->
		new UsernameNotFoundException("Pas de utilisateur "+ id));
		
		List<AppRole> roles = userRepository.getRoles(id);
		return UserPrincipal.create(user, roles);
	}

}
