package ci.kossovo.hotel.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.AppUser;

public interface UserRepository extends JpaRepository<AppUser, Long> {

	Optional<AppUser> findByEmail(String email);

	Optional<AppUser> findByUsernameOrEmail(String username, String email);

	List<AppUser> findByIdIn(List<Long> userIds);

	Optional<AppUser> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	Boolean existsByTitulaire(String cni);

	// liste des rôles d'un utilisateur identifié par son id
	@Query("select ur.role from UserRoles ur where ur.user.id=?1")
	List<AppRole> getRoles(Long id);

	// liste des rôles d'un utilisateur identifié par son login et son mot de passe
	@Query("select ur.role from UserRoles ur where ur.user.username=?1 and ur.user.password=?2")
	List<AppRole> getRoles(String username, String password);

}
