package ci.kossovo.hotel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.AppUser;
import ci.kossovo.hotel.entities.UserRoles;

public interface UserRoleRepository extends JpaRepository<UserRoles, Long> {
	
	Boolean existsByUserAndRole(AppUser user, AppRole role);
	Optional<UserRoles> findByUserAndRole(AppUser user, AppRole  role);

}
