package ci.kossovo.hotel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.num.NomRole;

public interface RoleRepository extends JpaRepository<AppRole, Long> {

	Optional<AppRole> findByNom(NomRole roleName);
	Optional<AppRole> findByNom(String nom);

	Boolean existsByNom(NomRole nomRole);

	Boolean existsByNom(String nom);

}
