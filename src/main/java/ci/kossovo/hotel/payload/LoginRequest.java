package ci.kossovo.hotel.payload;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

public class LoginRequest {

	@NotBlank
	@Getter @Setter
	private String usernameOrEmail;
	@NotBlank
	@Getter @Setter
	private String password;
}
