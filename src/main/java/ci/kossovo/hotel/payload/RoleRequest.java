package ci.kossovo.hotel.payload;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class RoleRequest {
	private Long idUser;
	private List<String> roles;
	
	
}
