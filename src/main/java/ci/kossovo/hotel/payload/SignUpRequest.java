package ci.kossovo.hotel.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter
@ToString
public class SignUpRequest {
	
	@NotBlank
	@Size(min=4, max=40)
	private String name;
	
	@NotBlank
	private String cni;
	
	@NotBlank
	@Size(min=3, max=20)
	private String username;
	
	@NotBlank
	@Size(max=40)
	@Email
	private String email;
	
	@NotBlank
	@Size(min=4, max=40)
	private String password;
	@NotBlank
	@Size(min=4, max=40)
	private String confirmPassword;

}
