package ci.kossovo.hotel.payload;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JwtAuthenticationResponse {

	private String accessToken;
	private String tokenType = "Hotel";

	public JwtAuthenticationResponse(String accessToken) {
		this.accessToken = accessToken;
	}

}
