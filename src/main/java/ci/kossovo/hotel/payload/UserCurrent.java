package ci.kossovo.hotel.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserCurrent {

	private Long id;
	private String name;
	private String username;
	private String email;
	private String password;

	
}
