package ci.kossovo.hotel;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import ci.kossovo.hotel.entities.AppRole;
import ci.kossovo.hotel.entities.num.NomRole;
import ci.kossovo.hotel.repository.RoleRepository;

@SpringBootApplication
@EnableJms
public class HotelSecuriteServiceApplication implements CommandLineRunner {
	private RoleRepository roleRepository;
	
	

	public HotelSecuriteServiceApplication(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(HotelSecuriteServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
		  if (!roleRepository.existsByNom(NomRole.ROLE_USER)) { roleRepository.save(new
		  AppRole(NomRole.ROLE_USER)); }
		  
		  if (!roleRepository.existsByNom(NomRole.ROLE_EMPLOYE)) {
		  roleRepository.save(new AppRole(NomRole.ROLE_EMPLOYE)); }
		  
		  if (!roleRepository.existsByNom(NomRole.ROLE_ADMIN)) {
		  roleRepository.save(new AppRole(NomRole.ROLE_ADMIN)); } if
		  (!roleRepository.existsByNom(NomRole.ROLE_GERANT)) { roleRepository.save(new
		  AppRole(NomRole.ROLE_GERANT)); } if
		  (!roleRepository.existsByNom(NomRole.ROLE_DBA)) { roleRepository.save(new
		  AppRole(NomRole.ROLE_DBA)); } if
		  (!roleRepository.existsByNom(NomRole.ROLE_CLIENT)) { roleRepository.save(new
		  AppRole(NomRole.ROLE_CLIENT)); } if
		  (!roleRepository.existsByNom(NomRole.ROLE_STAGIAIRE)) {
		  roleRepository.save(new AppRole(NomRole.ROLE_STAGIAIRE)); }
		 
		
	}

}

